(function ($) {

  Drupal.behaviors.forminputs = {
    attach: function (context, settings) {
      if($('#block-webform-client-block-1162',context).is('div')){
		$('#block-webform-client-block-1162',context).find('.form-text, .form-textarea').focus(function(){
			var tex = $(this).val();
			if(tex == 'subject' || tex == 'message' || tex == 'email'){
				$(this).val('');
			}
		}).blur(function(){
			var tex = $(this).val();
			if(tex == '' ){
				if($(this).hasClass('form-email')){
					$(this).val('email');
				}else if($(this).hasClass('form-text')){
					$(this).val('subject');
				}else{
					$(this).val('message');
				}
				
			}
		});
      }
    }
  };
  Drupal.behaviors.scroltopoem = {
    attach: function (context, settings) {
      if($('#block-views-poem-list-block',context).is('div')){
		var element = $('#block-views-poem-list-block .ui-accordion-content-active',context).find('a.active').parents('.views-row');
		var offset = element.offset();
		var offsetTop = offset.top;
		$('#block-views-poem-list-block .ui-accordion-content-active',context).animate({
				'scrollTop': offsetTop - 350 + 'px'
		}, 500);
      }
    }
  };
  // Drupal.behaviors.scroltoobservation = {
    // attach: function (context, settings) {
      // if($('#block-views-quotes-block-1',context).is('div') && $('.views-exposed-form').is('div') && ($('.views-exposed-form').find('#edit-name').val() != '' || $('.views-exposed-form').find('#edit-body-value').val() != '')){
      	// var QueryString = function () {
		  // // This function is anonymous, is executed immediately and 
		  // // the return value is assigned to QueryString!
		  // var query_string = {}; 
		  // var query = window.location.search.substring(1);
		  // var vars = query.split("&");
		  // for (var i=0;i<vars.length;i++) {
		    // var pair = vars[i].split("=");
		    	// // If first entry with this name
		    // if (typeof query_string[pair[0]] === "undefined") {
		      // query_string[pair[0]] = pair[1];
		    	// // If second entry with this name
		    // } else if (typeof query_string[pair[0]] === "string") {
		      // var arr = [ query_string[pair[0]], pair[1] ];
		      // query_string[pair[0]] = arr;
		    	// // If third or later entry with this name
		    // } else {
		      // query_string[pair[0]].push(pair[1]);
		    // }
		  // } 
		    // return query_string;
		// } ();
		// var name = QueryString.name;
		// name = name.replace('%20' , ' ');
		// var element = $('#block-views-quotes-block-1',context).find('a.' + QueryString.name).parents('.views-row');
		// $('#block-views-quotes-block-1',context).find('a.' + name).addClass('realactive');
		// var offset = element.offset();
		// var offsetTop = offset.top;
		// offsetTop = (offsetTop > 3120)? offsetTop + 100 : offsetTop - 100;
		// $('#block-views-quotes-block-1',context).animate({
				// 'scrollTop': offsetTop  + 'px'
		// }, 500);
      // }
    // }
  // };
  Drupal.behaviors.messages = {
    attach: function (context, settings) {
      if($('#messages',context).is('div')){
		$('#messages').click(function(){
			$(this).remove();
		});
      }
    }
  };
  Drupal.behaviors.frontline = {
    attach: function (context, settings) {
      if($('.front',context).is('body')){
		var hei = $('#block-views-cards-pack-block-2').outerHeight();
		var hei3 = $('#block-views-cards-pack-block-3').outerHeight();
		if(hei > hei3){
			$('#block-views-cards-pack-block-2').css('border-right','1px dashed #B7B7B7');
		}else{
			$('#block-views-cards-pack-block-3').css('border-left','1px dashed #B7B7B7');
		}
		
      }
    }
  };
  Drupal.behaviors.subjects = {
    attach: function (context, settings) {
      if($('#sidebar-first',context).is('div')){
      	var hei = $('#content',context).outerHeight();
      	// if($('.view-quotes',context).is('div')){
      		// //$('.view-quotes',context).css('min-height', hei + 'px');
      	// }else{
//       		
      	// }
      	$('#sidebar-first',context).css('height', hei - 42 + 'px');
      }
    }
  };
  Drupal.behaviors.subject = {
    attach: function (context, settings) {
      if($('.blocked-view',context).is('div')){
      	$('.blocked-view .item-list').each(function(){
      		var noofitems = $(this).find('ul').find('li').length;
      		if(noofitems > 4){
      			//var cont = $(this).html();
      			$(this).append('<div class="inner-row"></div>');
      			$(this).find('.inner-row').append($(this).find('h3'));
      			$(this).find('.inner-row').append($(this).find('ul'));
      			
      			$(this).find('ul').css('width', noofitems * 235 + 'px');
      			$(this).append('<div class="left-arrow"></div>');
      			$(this).prepend('<div class="right-arrow"></div>');
      			$(this).append('<div class="gal-pager"></div>');
      			var maxi = (noofitems * 235) - 940;
      			var page = Math.ceil(noofitems / 4);
      			for(i = 1; i <= page; ++i){
      				var active;
      				if(i === 1) {active = ' active';}else{active = '';}
      				$(this).find('.gal-pager').append('<div class="pager pager-' + i  + active + '"></div>');
      			}
      			
      			
	        
	        
      		}else{
      			$(this).find('ul').css('text-align','center');
      			$(this).find('li').css('float','none');
      		}
      		
      	});
      	var thenumber = 0;
      	$('.gal-pager').find('.pager').click(function(){
	  		var id = parseInt($(this).attr('class').replace('active','').replace('pager-','').replace('pager',''));	
	  		var idactive = parseInt($(this).parents('.item-list').find('.gal-pager').find('.pager.active').attr('class').replace('active','').replace('pager-','').replace('pager',''));	
  			if(id > idactive){
  				thenumber = id - idactive;
  				$(this).parents('.item-list').find('.right-arrow').click();
  				
  			}else{
  				thenumber = idactive - id;
  				$(this).parents('.item-list').find('.left-arrow').click();
  			}
  		});
      	$('.right-arrow', context).click(function(){
      		var maxi = ( $(this).parent().find('.inner-row').find('ul').find('li').length * 235) - 940;
			var lef = parseInt($(this).parent().find('.inner-row').find('ul').css('margin-left'));
        	$(this).parent().find('.inner-row').find('ul').animate({'margin-left': Math.max(-maxi, lef-940)},700,function(){
        		if(thenumber > 1){
		   			thenumber--;
		   			$(this).parents('.item-list').find('.right-arrow').click();
		   		}
        	});
	   		if($(this).parents('.item-list').find('.gal-pager').find('.pager.active').next().is('div')){
	   			$(this).parents('.item-list').find('.gal-pager').find('.pager.active').removeClass('active').next().addClass('active');
	   		}
	   		
	   		
	  	});
        
        $('.left-arrow', context).click(function(){
        	var lef = parseInt($(this).parent().find('.inner-row').find('ul').css('margin-left'));
        	$(this).parent().find('.inner-row').find('ul').animate({'margin-left': Math.min(0,lef+940)},700,function(){
        		if(thenumber > 1){
		   			thenumber--;
		   			$(this).parents('.item-list').find('.left-arrow').click();
		   		}
        	});
	       if( $(this).parent().find('.gal-pager').find('.pager.active').prev().is('div')){
	       	$(this).parent().find('.gal-pager').find('.pager.active').removeClass('active').prev().addClass('active');
	       }
	        
        });
        
      }
    }
  };


}(jQuery));