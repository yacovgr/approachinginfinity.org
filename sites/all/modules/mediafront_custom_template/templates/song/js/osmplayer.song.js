(function(template, osmplayer) {

  /** The osmplayer namespace. */
  var osmplayer = osmplayer || {};

  // templated player.
  osmplayer[template] = function(context, options) {

    // Make sure we provide default options...
    this.options = jQuery.extend({
      controllerOnly: false
    }, this.options);

    // Derive from osmplayer.
    osmplayer.call(this, context, options);
  };

  /**
   * Define this template prototype.
   */
  osmplayer[template].prototype = new osmplayer();
  osmplayer[template].prototype.constructor = osmplayer[template];

  /**
   * The player constructor.
   */
  osmplayer[template].prototype.construct = function() {
    osmplayer.prototype.construct.call(this);
    if (this.options.controllerOnly) {
      this.display.addClass('controller-only');
    }
  };

/**
 * Plays the next media file in the queue.
 */
	osmplayer[template].prototype.playNext = function() {
	  if (this.playQueue.length > this.playIndex) {
	    this.load(this.playQueue[this.playIndex]);
	    this.playIndex++;
	  }
	  else if (this.options.repeat) {
	    this.playIndex = 0;
	    this.playNext();
	     this.media.play();
	  }
	  else if (this.playQueue.length > 0) {
	    // If there is no playlist, and no repeat, we will
	    // just seek to the beginning and pause.
	    this.options.autoplay = false;
	    this.playIndex = 0;
	    this.playNext();
	    this.media.play();
	  }
	  else if (this.media) {
	    // Stop the player and unload.
	    this.media.stop();
	  }
	};


  /**
   * Return the display for this plugin.
   */
  osmplayer[template].prototype.getDisplay = function() {

    // If this is the bottom element, then we need to build.
    if (this.context.children().length == 0) {

      // Build out the player provided the base tag.
      this.context = this.context.attr({
        'id': this.options.id + '-player',
        'class': 'minplayer-' + template + '-media'
      })
      .wrap(jQuery(document.createElement('div')).attr({
        'class': 'minplayer-' + template + '-display ui-widget-content'
      })).parent('.minplayer-' + template + '-display')
      .wrap(jQuery(document.createElement('div')).attr({
        'class': 'minplayer-' + template
      })).parent('.minplayer-' + template)
      .prepend('\
        <div class="minplayer-' + template + '-logo"></div>\
        <div class="minplayer-' + template + '-error"></div>'
      )
      .wrap(jQuery(document.createElement('div')).attr({
        'id': this.options.id,
        'class': 'osmplayer-' + template + ' player-ui'
      })).parent('.osmplayer-' + template);

      // Mark a flag that says this display needs to be built.
      this.options.build = true;
    }

    return this.context;
  }
	
  osmplayer[template].prototype.loadNode = function(node) {
  	osmplayer.prototype.loadNode.call(this, node);
  		this.elements.body.html(node.body);
  		
  };
	
  // Get the elements for this player.
  osmplayer[template].prototype.getElements = function() {
    var elements = osmplayer.prototype.getElements.call(this);

    // Set the width and height of this element.
    this.display.width(this.options.width);
    this.display.height(this.options.height);

    // Get the minplayer component.
    var minplayer = jQuery('.minplayer-' + template, this.display);
    if (this.options.playlistOnly) {
      minplayer.remove();
      minplayer = null;
    }

    return jQuery.extend(elements, {
      player:this.display,
      minplayer: minplayer,
      body:jQuery('.minplayer-' + template + '-body', this.display),
      display:jQuery('.minplayer-' + template + '-display', this.display),
      media:jQuery('.minplayer-' + template + '-media', this.display),
      error:jQuery('.minplayer-' + template + '-error', this.display),
      logo:jQuery('.minplayer-' + template + '-logo', this.display)
    });
  };
})('song', osmplayer);
